use std::path::Path;

/// Scan a file for lines containing `#include "some/path"` and make sure
/// the specified path refers to a file relative to the source directory.
pub fn find_invalid_includes<'a>(
    source_dir: &Path,
    lines: impl Iterator<Item = &'a String>,
) -> Vec<Violation> {
    lines
        .enumerate()
        .filter_map(|(line, content)| {
            extract_relative_include(content).and_then(|include_path| {
                if !source_dir.join(include_path).is_file() {
                    Some(Violation {
                        line,
                        content: content.to_owned(),
                    })
                } else {
                    None
                }
            })
        })
        .collect()
}

/// Record details of a violation in a file
#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct Violation {
    /// Zero-based index of the problematic line
    pub line: usize,
    /// Content of the line
    pub content: String,
}

impl Violation {
    /// Return the corrected version of the problematic line
    pub fn correction(&self) -> String {
        // we know the problematic line is something like
        // `  #  include  "some/path/to/header.h"  `
        // so we just replace the first `"` with `<` and the second with `>`.
        self.content.replacen('"', "<", 1).replacen('"', ">", 1)
    }
}

pub fn extract_relative_include(line: &str) -> Option<&str> {
    line.trim_start()
        .strip_prefix('#') // allow for spaces before `#`
        .and_then(|line| line.trim_start().strip_prefix("include")) // look for `<spaces>include`
        .and_then(|line| line.trim_start().strip_prefix('"')) // look for `<spaces>"`
        .and_then(|line| line.find('"').map(|pos| &line[0..pos])) // look for closing `"` and return the slice before
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    use super::*;

    #[test]
    fn parse_includes() {
        // normal cases
        assert_eq!(extract_relative_include("#include <string>"), None);
        assert_eq!(
            extract_relative_include("#include \"my_header.h\""),
            Some("my_header.h")
        );

        // handling of spaces
        assert_eq!(
            extract_relative_include("  #    include    <string>   "),
            None
        );
        assert_eq!(
            extract_relative_include("  #    include       \"my_header.h\"   // comment"),
            Some("my_header.h")
        );

        // uninteresting lines
        assert_eq!(extract_relative_include("int main() {}"), None);
        assert_eq!(
            extract_relative_include("// #include \"my_header.h\""),
            None
        );

        // invalid lines
        assert_eq!(extract_relative_include("#include \"my_header.h"), None);
    }

    #[test]
    fn scan_file_content() {
        let this_file = PathBuf::from(file!());
        let source_dir = this_file.parent().unwrap();
        assert_eq!(
            find_invalid_includes(
                source_dir,
                vec![
                    String::from("#include <string>"),
                    String::from("#include \"main.rs\""),
                    String::from("#include \"remote_header.h\""),
                ]
                .iter()
            ),
            vec![Violation {
                line: 2,
                content: String::from("#include \"remote_header.h\""),
            }]
        );
    }

    #[test]
    fn violation_correction() {
        let check = |content: &str, correction: &str| {
            assert_eq!(
                Violation {
                    line: 0,
                    content: content.to_owned(),
                }
                .correction(),
                correction
            );
        };
        // regular case
        check("#include \"header.h\"", "#include <header.h>");
        // weird cases
        check(
            " #  include    \" strange name \"  // \"comment\" ",
            " #  include    < strange name >  // \"comment\" ",
        );
    }
}
