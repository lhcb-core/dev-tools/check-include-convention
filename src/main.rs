use check_include_convention::{find_invalid_includes, Violation};
use clap::Parser;
use std::{
    path::{Path, PathBuf},
    process::exit,
};

#[derive(Debug, Parser)]
#[clap(version, about)]
struct Args {
    /// Files to check/fix
    filenames: Vec<PathBuf>,
    /// If passed, modify the files in place
    #[clap(short, long, default_value_t = false)]
    in_place: bool,
    /// Do not print messages about the changes
    #[clap(short, long, default_value_t = false)]
    quiet: bool,
}

fn main() {
    let args = Args::parse();
    let report = args
        .filenames
        .into_iter()
        .filter_map(|p| {
            let report = process(&p, args.in_place);
            if report.as_ref().is_ok_and(|r| r.is_empty()) {
                None
            } else {
                Some((p, report))
            }
        })
        .collect::<Vec<_>>();
    if report.is_empty() {
        if !args.quiet {
            println!("no problem");
        }
    } else {
        // if called to modify we print only the errors
        // if we are not changing the files we print what we would change
        let report = if args.quiet {
            report
                .into_iter()
                .filter_map(|(p, r)| if r.is_err() { Some((p, r)) } else { None })
                .collect()
        } else {
            report
        };
        if !report.is_empty() {
            println!("found problems:");
        }
        report.into_iter().for_each(|(path, report)| {
            println!("  {}:", path.display());
            if let Ok(violations) = report {
                violations.into_iter().for_each(|violation| {
                    println!(
                        "    {}:\n      <: {}\n      >: {}",
                        violation.line + 1,
                        violation.content,
                        violation.correction()
                    );
                });
            } else {
                println!("    error: {:?}", report.err());
            }
        });
        exit(1);
    }
}

/// Scan a file for lines containing `#include "some/path"` and make sure
/// the specified path refers to a file relative to the source directory.
fn process(path: &Path, modify: bool) -> std::io::Result<Vec<Violation>> {
    use std::fs::File;
    use std::io::{BufRead, BufReader, BufWriter, Write};
    let mut lines = BufReader::new(File::open(path)?)
        .lines()
        .map(|line| line.map(String::from))
        .collect::<std::io::Result<Vec<String>>>()?;
    let source_dir = path.parent().unwrap(); // no panic as we managed to open the file
    let violations = find_invalid_includes(source_dir, lines.iter());
    if modify {
        for violation in &violations {
            *lines.get_mut(violation.line).unwrap() = violation.correction();
        }
        let mut buffer = BufWriter::new(File::create(path)?);
        for line in lines {
            writeln!(buffer, "{}", line)?;
        }
        buffer.flush()?; // BufWriter docs suggests to do an explicit flush at the end
    }
    Ok(violations)
}
