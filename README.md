# Check `#include` for *correct* use of `<>` and `""`

A small, [`pre-commit`](https://pre-commit.com) compatible, command line tool to sanitize the uses of
the C preprocessor directive `#include`.

We ensure that when using `#include "some_header.h"` the string refers to a path relative to the current
source file, otherwise we should use `#include <some_header.h>`.


## Rationale
According to the [cppreference page](https://en.cppreference.com/w/cpp/preprocessor/include), there
is no difference between
```cpp
#include <some_header.h>
```
and
```cpp
#include "some_header.h"
```
except for a cryptic mention that the first is for *headers* and the second for *sources*, but it works
for headers too.

In the [notes](https://en.cppreference.com/w/cpp/preprocessor/include#Notes) it is explained that
*typical implementations* (i.e. it is implementation specific but everybody does the same) search
for the file to include in the same path as the source file being processed and if not found it falls
back to the same logic used for the first form (i.e. an implementation defined search path).

This can be rephrased saying that when you use `#include "some_header.h"` you are signalling the
compiler that you mean a path relative to the current source, while the form
`#include <some_header.h>` means you do not want to consider a path relative to the current source
(of course, you can invalidate this argument by adding `-Ipath/to/source/dir` before other
headers directories in the compiler command invocation).
